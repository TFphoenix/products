#include "PerishableProduct.h"

PerishableProduct::PerishableProduct(int32_t id, const std::string& name, float rawPrice, const std::string& expirationDate) : m_expirationDate(expirationDate), Product(id, name, rawPrice) {}

#pragma region Getters
const std::string& PerishableProduct::getExpirationDate() const
{
	return m_expirationDate;
}
float PerishableProduct::GetPrice() const
{
	return m_rawPrice + static_cast<float>(GetVAT())* m_rawPrice / 100;
}

uint16_t PerishableProduct::GetVAT() const
{
	return m_vat;
}
#pragma endregion Getters