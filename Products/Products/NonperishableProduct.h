#pragma once
#include "Product.h"

class NonperishableProduct :public Product
{
public:
	enum class Type
	{
		Clothing,
		SmallAppliences,
		PersonalHygiene
	};

public:
	NonperishableProduct(int32_t id, const std::string& name, float rawPrice, Type type);
	~NonperishableProduct() override = default;

	Type GetType() const;
	float GetPrice() const override final;
	uint16_t GetVAT() const override final;

private:
	const static uint16_t m_vat = 19;
	Type m_type;
};