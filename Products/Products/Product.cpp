#include "Product.h"

Product::Product(uint16_t id, const std::string& name, float rawPrice) : m_id(id), m_name(name), m_rawPrice(rawPrice)
{
	// empty
}

#pragma region Getters
uint16_t Product::GetID() const
{
	return m_id;
}
const std::string& Product::GetName() const
{
	return m_name;
}
float Product::GetRawPrice() const
{
	return m_rawPrice;
}
#pragma endregion Getters