#include "NonperishableProduct.h"

NonperishableProduct::NonperishableProduct(int32_t id, const std::string& name, float rawPrice, Type type) :m_type(type), Product(id, name, rawPrice) {}

#pragma region Getters
NonperishableProduct::Type NonperishableProduct::GetType() const
{
	return m_type;
}
float NonperishableProduct::GetPrice() const
{
	return m_rawPrice + static_cast<float>(GetVAT())* m_rawPrice / 100;
}

uint16_t NonperishableProduct::GetVAT() const
{
	return m_vat;
}
#pragma endregion Getters
