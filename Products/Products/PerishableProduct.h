#pragma once
#include "Product.h"

class PerishableProduct :public Product
{
public:
	PerishableProduct(int32_t id, const std::string& name, float rawPrice, const std::string& expirationDate);
	~PerishableProduct() override = default;

	const std::string& getExpirationDate() const;
	float GetPrice() const final;
	uint16_t GetVAT() const final;

private:
	const static uint16_t m_vat = 9;
	std::string m_expirationDate;
};