#pragma once
#include <string>
#include <cstdint>
#include "IPriceable.h"

class Product :public IPriceable
{
public:
	Product(uint16_t id, const std::string& name, float rawPrice);

	uint16_t GetID() const;
	const std::string& GetName() const;
	float GetRawPrice() const;

protected:
	uint16_t m_id;
	std::string m_name;
	float m_rawPrice;
};